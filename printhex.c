#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

void prompt(char text[100]) {
    printf("Name of input file > ");
    fgets(text, 100, stdin);
    int x = strlen(text);
    text[x-1] = '\0';
}

char convert(char *text) {
    char hex = '-';
    if (strcmp(text, "0000") == 0) hex = '0';
    else if (strcmp(text, "0001") == 0) hex = '1';
    else if (strcmp(text, "0010") == 0) hex = '2';
    else if (strcmp(text, "0011") == 0) hex = '3';
    else if (strcmp(text, "0100") == 0) hex = '4';
    else if (strcmp(text, "0101") == 0) hex = '5';
    else if (strcmp(text, "0110") == 0) hex = '6';
    else if (strcmp(text, "0111") == 0) hex = '7';
    else if (strcmp(text, "1000") == 0) hex = '8';
    else if (strcmp(text, "1001") == 0) hex = '9';
    else if (strcmp(text, "1010") == 0) hex = 'a';
    else if (strcmp(text, "1011") == 0) hex = 'b';
    else if (strcmp(text, "1100") == 0) hex = 'c';
    else if (strcmp(text, "1101") == 0) hex = 'd';
    else if (strcmp(text, "1110") == 0) hex = 'e';
    else if (strcmp(text, "1111") == 0) hex = 'f';
    return hex;
}

void set(char *x) {
  char z[11] = "0000000000";
  int y = strlen(x);
  z[10-y] = '\0';
  strcat(z, x);
  strcpy(x, z);
}

void printHex(char x, int y) {
    char offset[11];
    offset[10] = '\0';
    sprintf(offset, "%d", y);
    set(offset);
    //printf("%s ", offset);
    if (x == '-') printf("%s ", offset);
    else if (y%8 == 0) printf("%c ", x);
    else if (y%8 != 0) printf("%c", x);
    if (y%128 == 0 && y != 0) {
      printf("\n");
      printf("%s ", offset);
    }
}

void load (char *path){
    FILE *in = fopen(path, "rb");
    char x;
    char y[5];
    y[4] = '\0';
    int i = 0;
    char z;
    int offset = 0;
    printHex('-', offset);
    if (in) {
      x = fgetc(in);
      while (! feof(in)) {
        offset++;
        y[i] = x;
        x = fgetc(in);
        if (i == 3) {
          z = convert(y);
          i = -1;
          printHex(z, offset);
        }
        i++;
      }
      printf("\n");
      printHex('-', offset);
      fclose(in);
    }
    else printf("File not found\n");
}

int main(int n, char *args[n]) {
    char text[100];
    prompt(text);
    load(text);
    return 0;
}
